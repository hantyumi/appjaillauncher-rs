#![cfg(windows)]

#[allow(unused_imports)]
use log::*;
use std::env;
use std::ffi::OsStr;
use std::fs;
use std::iter::once;
use std::mem;
use std::os::windows::ffi::OsStrExt;
use std::path::{Path, PathBuf};
use utils::{HandlePtr, WindowsProcess};
use winapi::shared::minwindef::{DWORD, LPBYTE, LPVOID, WORD};
use winapi::shared::ntdef::{NULL, PVOID};
use winapi::shared::winerror::{
    ERROR_ALREADY_EXISTS, ERROR_FILE_NOT_FOUND, ERROR_INVALID_PARAMETER, ERROR_SUCCESS,
    HRESULT_FROM_WIN32,
};
use winapi::um::errhandlingapi::GetLastError;
use winapi::um::handleapi::{SetHandleInformation, INVALID_HANDLE_VALUE};
use winapi::um::minwinbase::LPSECURITY_ATTRIBUTES;
use winapi::um::processthreadsapi::{
    CreateProcessW, InitializeProcThreadAttributeList, UpdateProcThreadAttribute,
    LPPROC_THREAD_ATTRIBUTE_LIST, LPSTARTUPINFOW, PPROC_THREAD_ATTRIBUTE_LIST, PROCESS_INFORMATION,
    STARTUPINFOW,
};
use winapi::um::securitybaseapi::FreeSid;
use winapi::um::userenv::DeriveAppContainerSidFromAppContainerName;
use winapi::um::winbase::{
    EXTENDED_STARTUPINFO_PRESENT, HANDLE_FLAG_INHERIT, LPSTARTUPINFOEXW, STARTF_USESHOWWINDOW,
    STARTF_USESTDHANDLES, STARTUPINFOEXW,
};
use winapi::um::winnt::{
    HANDLE, HRESULT, LPWSTR, PSECURITY_CAPABILITIES, PSID, PSID_AND_ATTRIBUTES,
    SECURITY_CAPABILITIES, SE_GROUP_ENABLED, SID_AND_ATTRIBUTES,
};
use winapi::um::winuser::SW_HIDE;
use winapi::{
    shared::basetsd::{PSIZE_T, SIZE_T},
    um::{
        processthreadsapi::{GetCurrentProcess, OpenProcessToken},
        userenv::{CreateEnvironmentBlock, DestroyEnvironmentBlock},
        winbase::CREATE_UNICODE_ENVIRONMENT,
        winnt::{TOKEN_DUPLICATE, TOKEN_QUERY},
    },
};
use windows_acl::helper::{sid_to_string, string_to_sid};
use winffi::*;

#[cfg(test)]
use winapi::um::fileapi;
#[cfg(test)]
use winapi::um::namedpipeapi;
#[cfg(test)]
use winapi::um::synchapi;

#[allow(dead_code)]
pub struct Profile {
    profile: String,
    outbound_network: bool,
    debug: bool,
    pub sid: String,
}

struct EnvBlock {
    ptr: LPVOID,
}

// Generated randomly, must only contain these characters: 1234567890abcdefghjkmnpqrstvwxyzABCDEFGHJKMNPQRSTVWXYZ
static PACKAGE_PUBLISHER_ID: &str = "adbf2svj6dhj2";

// Since we use container type 3, we need to add a publisher ID to the profile name
fn get_full_profile(profile: &str) -> String {
    profile.to_owned() + "_" + PACKAGE_PUBLISHER_ID
}

fn get_package_path(profile: &str) -> Result<PathBuf, HRESULT> {
    let local_app_data =
        env::var("LOCALAPPDATA").map_err(|_| HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND))?;
    Ok(Path::new(&local_app_data).join("Packages").join(&profile))
}

#[allow(dead_code)]
impl Profile {
    pub fn new(profile: &str) -> Result<Profile, HRESULT> {
        // Underscores aren't allowed in the profile name
        if profile.contains("_") {
            error!("Profile names cannot contain underscores");
            return Err(HRESULT_FROM_WIN32(ERROR_INVALID_PARAMETER));
        }

        let profile = get_full_profile(profile);

        let mut p_sid: PSID = 0 as PSID;
        let profile_name: Vec<u16> = OsStr::new(&profile).encode_wide().chain(once(0)).collect();

        // Create new folder in %LOCALAPPDATA%\Packages\ to make container type 3 work
        fs::create_dir_all(get_package_path(&profile)?)
            .map_err(|_| HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND))?;

        let hr = unsafe {
            CreateAppContainerProfileWorker(
                profile_name.as_ptr(),
                profile_name.as_ptr(),
                profile_name.as_ptr(),
                NULL as PSID_AND_ATTRIBUTES,
                0 as DWORD,
                3 as DWORD,
                &mut p_sid,
            )
        };

        if hr == HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS) {
            let hr = unsafe {
                DeriveAppContainerSidFromAppContainerName(profile_name.as_ptr(), &mut p_sid)
            };
            if hr != (ERROR_SUCCESS as HRESULT) {
                return Err(hr);
            }
        }

        let string_sid = match sid_to_string(p_sid) {
            Ok(x) => x,
            Err(x) => return Err(x as HRESULT),
        };

        unsafe { FreeSid(p_sid) };

        Ok(Profile {
            profile: profile,
            outbound_network: true,
            debug: false,
            sid: string_sid,
        })
    }

    fn remove(&self) -> bool {
        let profile_name: Vec<u16> = OsStr::new(&self.profile)
            .encode_wide()
            .chain(once(0))
            .collect();

        let hr = unsafe { DeleteAppContainerProfileWorker(profile_name.as_ptr(), 3) };
        if hr != (ERROR_SUCCESS as HRESULT) {
            return false;
        }

        let package_path = get_package_path(&self.profile);
        if !package_path.is_ok() {
            return false;
        }

        fs::remove_dir_all(package_path.unwrap()).is_ok()
    }

    pub fn enable_outbound_network(&mut self, has_outbound_network: bool) {
        self.outbound_network = has_outbound_network;
    }

    pub fn enable_debug(&mut self, is_debug: bool) {
        self.debug = is_debug;
    }

    pub fn launch(
        &self,
        stdin: HandlePtr,
        stdout: HandlePtr,
        dir_path: &str,
        child_path: &str,
    ) -> Result<WindowsProcess, DWORD> {
        let network_allow_sid = string_to_sid("S-1-15-3-1")?;
        let sid = string_to_sid(&self.sid)?;
        let mut capabilities = SECURITY_CAPABILITIES {
            AppContainerSid: sid.as_ptr() as PSID,
            Capabilities: 0 as PSID_AND_ATTRIBUTES,
            CapabilityCount: 0,
            Reserved: 0,
        };
        let mut attrs;
        let mut si = STARTUPINFOEXW {
            StartupInfo: STARTUPINFOW {
                cb: 0 as DWORD,
                lpReserved: NULL as LPWSTR,
                lpDesktop: NULL as LPWSTR,
                lpTitle: NULL as LPWSTR,
                dwX: 0 as DWORD,
                dwY: 0 as DWORD,
                dwXSize: 0 as DWORD,
                dwYSize: 0 as DWORD,
                dwXCountChars: 0 as DWORD,
                dwYCountChars: 0 as DWORD,
                dwFillAttribute: 0 as DWORD,
                dwFlags: 0 as DWORD,
                wShowWindow: 0 as WORD,
                cbReserved2: 0 as WORD,
                lpReserved2: NULL as LPBYTE,
                hStdInput: 0 as HANDLE,
                hStdOutput: 0 as HANDLE,
                hStdError: 0 as HANDLE,
            },
            lpAttributeList: 0 as PPROC_THREAD_ATTRIBUTE_LIST,
        };
        let mut creation_flags: DWORD = 0 as DWORD;
        let mut attr_buf: Vec<u8>;

        if !self.debug {
            debug!("Setting up AppContainer");

            if self.outbound_network {
                debug!("Setting up SID_AND_ATTRIBUTES for outbound network permissions");

                attrs = SID_AND_ATTRIBUTES {
                    Sid: network_allow_sid.as_ptr() as PSID,
                    Attributes: SE_GROUP_ENABLED,
                };

                capabilities.CapabilityCount = 1;
                capabilities.Capabilities = &mut attrs;
            }

            let mut list_size: SIZE_T = 0;
            if unsafe {
                InitializeProcThreadAttributeList(
                    0 as LPPROC_THREAD_ATTRIBUTE_LIST,
                    1,
                    0,
                    &mut list_size,
                )
            } != 0
            {
                debug!(
                    "InitializeProcThreadAttributeList failed: GLE={:}",
                    unsafe { GetLastError() }
                );
                return Err(unsafe { GetLastError() });
            }

            attr_buf = Vec::with_capacity(list_size as usize);
            if unsafe {
                InitializeProcThreadAttributeList(
                    attr_buf.as_mut_ptr() as LPPROC_THREAD_ATTRIBUTE_LIST,
                    1,
                    0,
                    &mut list_size,
                )
            } == 0
            {
                debug!(
                    "InitializeProcThreadAttributeList failed: GLE={:}",
                    unsafe { GetLastError() }
                );
                return Err(unsafe { GetLastError() });
            }

            if unsafe {
                UpdateProcThreadAttribute(
                    attr_buf.as_mut_ptr() as LPPROC_THREAD_ATTRIBUTE_LIST,
                    0,
                    PROC_THREAD_ATTRIBUTE_SECURITY_CAPABILITIES,
                    mem::transmute::<PSECURITY_CAPABILITIES, LPVOID>(&mut capabilities),
                    mem::size_of::<SECURITY_CAPABILITIES>() as SIZE_T,
                    0 as PVOID,
                    0 as PSIZE_T,
                )
            } == 0
            {
                debug!("UpdateProcThreadAttribute failed: GLE={:}", unsafe {
                    GetLastError()
                });
                return Err(unsafe { GetLastError() });
            }

            si.StartupInfo.cb = mem::size_of::<STARTUPINFOEXW>() as DWORD;
            si.lpAttributeList = attr_buf.as_mut_ptr() as PPROC_THREAD_ATTRIBUTE_LIST;

            creation_flags |= EXTENDED_STARTUPINFO_PRESENT;
        } else {
            debug!("Debug mode -- no extended STARTUPINFO");
            si.StartupInfo.cb = mem::size_of::<STARTUPINFOW>() as DWORD;
        }

        si.StartupInfo.dwFlags = STARTF_USESHOWWINDOW;
        si.StartupInfo.wShowWindow = SW_HIDE as WORD;

        let current_dir: Vec<u16> = OsStr::new(dir_path).encode_wide().chain(once(0)).collect();

        // Get env block from current user's base environment
        let env_block = {
            // Grab the token from the process
            let mut raw_token_handle: HANDLE = INVALID_HANDLE_VALUE;
            let status = unsafe {
                OpenProcessToken(
                    GetCurrentProcess(),
                    TOKEN_QUERY | TOKEN_DUPLICATE,
                    &mut raw_token_handle,
                )
            };
            if status == 0 {
                println!("OpenProcessToken failed: GLE={:}", unsafe {
                    GetLastError()
                });
                return Err(unsafe { GetLastError() });
            }

            let token_handle = HandlePtr::new(raw_token_handle);

            // Create environment block
            creation_flags |= CREATE_UNICODE_ENVIRONMENT;
            EnvBlock::new(token_handle.raw)?
        };

        if stdout.raw != INVALID_HANDLE_VALUE && stdin.raw != INVALID_HANDLE_VALUE {
            si.StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
            si.StartupInfo.hStdInput = stdin.raw as HANDLE;
            si.StartupInfo.hStdOutput = stdout.raw as HANDLE;
            si.StartupInfo.hStdError = stdout.raw as HANDLE;

            // Ensure the handle is inheritable
            if unsafe { SetHandleInformation(stdin.raw, HANDLE_FLAG_INHERIT, 1) } == 0 {
                return Err(unsafe { GetLastError() });
            }

            if stdin.raw != stdout.raw {
                if unsafe { SetHandleInformation(stdout.raw, HANDLE_FLAG_INHERIT, 1) } == 0 {
                    return Err(unsafe { GetLastError() });
                }
            }
        }

        let cmd_line: Vec<u16> = OsStr::new(child_path)
            .encode_wide()
            .chain(once(0))
            .collect();
        let mut pi = PROCESS_INFORMATION {
            hProcess: 0 as HANDLE,
            hThread: 0 as HANDLE,
            dwProcessId: 0 as DWORD,
            dwThreadId: 0 as DWORD,
        };

        if unsafe {
            CreateProcessW(
                cmd_line.as_ptr(),
                NULL as LPWSTR,
                NULL as LPSECURITY_ATTRIBUTES,
                NULL as LPSECURITY_ATTRIBUTES,
                1,
                creation_flags,
                env_block.ptr,
                current_dir.as_ptr(),
                mem::transmute::<LPSTARTUPINFOEXW, LPSTARTUPINFOW>(&mut si),
                &mut pi,
            )
        } == 0
        {
            println!("CreateProcess failed: GLE={:}", unsafe { GetLastError() });
            return Err(unsafe { GetLastError() });
        }

        debug!("  Child PID = {:}", pi.dwProcessId);

        // Create handle wrapper for thread to close when this function returns
        let _thread = HandlePtr::new(pi.hThread);

        // Create process wrapper - will be killed on drop
        let process =
            WindowsProcess::new(HandlePtr::new(pi.hProcess), pi.dwProcessId, stdin, stdout);

        Ok(process)
    }
}

impl Drop for Profile {
    fn drop(&mut self) {
        self.remove();
    }
}

impl EnvBlock {
    fn new(token: HANDLE) -> Result<EnvBlock, DWORD> {
        let mut env_block_ptr: LPVOID = NULL as LPVOID;
        let status = unsafe { CreateEnvironmentBlock(&mut env_block_ptr, token, 0) };
        if status == 0 {
            println!("CreateEnvironmentBlock failed: GLE={:}", unsafe {
                GetLastError()
            });
            return Err(unsafe { GetLastError() });
        }

        Ok(EnvBlock { ptr: env_block_ptr })
    }
}

impl Drop for EnvBlock {
    fn drop(&mut self) {
        unsafe {
            DestroyEnvironmentBlock(self.ptr);
        }
    }
}
